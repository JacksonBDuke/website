# README #

This is the original repository for my personal website.
This repository is no longer being updated and is only for reference on the site's progress.
Please visit https://bitbucket.org/JacksonBDuke/website2/overview for the current repository.
The live version can be seen at https://www.jackson-duke.com/